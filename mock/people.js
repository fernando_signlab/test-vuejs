import Mock from 'mockjs'

const data = Mock.mock({
  'items|30': [{
    id: '@id',
    first_name: '@first',
    last_name: '@name',
    web: '@url',
    pageviews: '@integer(300, 5000)',
    last_connection: '@datetime'
  }]
})

export default [
  {
    url: '/people/list',
    type: 'get',
    response: config => {
      const items = data.items
      return {
        code: 20000,
        data: {
          total: items.length,
          items: items
        }
      }
    }
  }
]
