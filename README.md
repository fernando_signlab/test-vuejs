# Prueba de concepto - Signlab

Esta prueba está basada en el panel de administración: https://github.com/PanJiaChen/vue-element-admin
Para los componentes se utiliza https://element.eleme.io/2.7 (Ya está instalada)

Primeros pasos:
- Clonar el proyecto: https://gitlab.com/fernando_signlab/test-vuejs.git
- Instalar los paquetes: npm/yarn install
- Lanzar el proyecto: npm/yarn run dev

## Primer objetivo: Listado de personas
El primer objetivo es crear un nuevo elemento en el menú llamado "people", será una tabla con un listado de personas. (Hay un ejemplo creado con una tabla simple)

Ayuda: En la carpeta mock se encuentra el endpoint que mockea los datos. El primer paso sería añadir en la carpeta api un nuevo archivo people.js con la funcionalidad necesaria para que consuma dicho endpoint.

Lo siguiente es añadir el elemento al menú lateral en el archivo src/router/index.js

1. Sacar una tabla con la lista de personas cuyas columnas son:
- first_name
- last_name
- web => Debe tener un enlace para llevar a la web
- pageviews
- last_connection

## Mejorando la tabla: paginación
El segundo objetivo es realizar una paginación de la tabla people. con el componente https://element.eleme.io/2.7/#/es/component/pagination
- 10 elementos por página

## Simulación de edición
En este apartado se procederá a crear una nueva columna en la tabla que contendrá un botón de edición. Al pulsar dicho botón, mostrará un dialog con un formulario, que se rellenará con la información de la persona.

Lo que hay que hacer:
- Se deberá Añadir una nueva columna a la tabla de people que incluya un botón de edición. Dicho botón abrirá un modal (Está instalada la biblioteca https://github.com/euvl/vue-js-modal)
- Para el modal, se creará un componente llamado <person-modal> en components y se incluirá en app.vue justo debajo de <router-view />
    En dicho modal habrá un formulario que contenga los campos:
    - first_name
    - last_name
    - web

El modal contendrá un botón de guardar que al pulsar tan sólo mostrará una notificación indicando que se ha guardado correctamente. (https://element.eleme.io/2.7/#/es/component/notification)

Cualquier duda, enviar un correo a fernando@signlab.es